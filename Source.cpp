#include <iostream>
#include "List.h"

using namespace std;

void main() 
{
	List<int> example;
	example.push_front(5);
	example.push_front(10);
	example.push_front(15);
	example.push_back(20);
	example.push_back(30);
	example.show();
	system("pause");
}