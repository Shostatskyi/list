#include <iostream>

template <class T>
struct Node {
	T data;
	Node *next;
	Node *prev;
};

template <class T>
class List {
private:   
	Node<T> *head;
	Node<T> *tail;
	size_t  size;

public:
	List(): head(NULL), tail(NULL), size(0) {}
	~List();
	void show();
	void push_front(T);
	void push_back(T);
};

template <class T>
List<T>::~List()
{
	for (; size--; delete (*head).prev, head = (*head).next);
}

template <class T>
void List<T>::push_front(T val) 
{
	Node<T> *n =  new Node<T>;
	n->data = val;
	n->next = head;
	n->prev = NULL; 
	if (size) head->prev = n;
	else tail = n;
	head = n;
	size++;
}

template <class T>
void List<T>::push_back(T val)
{
	Node<T> *n = new Node<T>;
	n->data = val;
	n->prev = tail;
	n->next = NULL; 
	if (size) tail->next = n;
	else head = n;
	tail = n;
	size++;
}

template <class T>
void List<T>::show()
{
	Node<T> *p1 = head, *p2 = tail;

	for (int i = 0; i < size; i++, head = (*head).next)
		std::cout << (*head).data << ' ';

	std::cout << std::endl;

	for (int i = 0; i < size; i++, tail = (*tail).prev)
		std::cout << (*tail).data << ' ';

	std::cout << std::endl;

	head = p1, tail = p2;
}

